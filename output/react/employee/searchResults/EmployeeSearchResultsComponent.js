import React from 'react';
import WindowContainer from '../../../common/layoutComponents/widgets/WindowContainer';
import { IntlProvider, FormattedMessage, injectIntl } from 'react-intl';
import UseTableSorter from '../../../common/layoutComponents/tableComponents/UseTableSorter'
import { StyledTable } from '../../../common/layoutComponents/widgets/TableComponent'
import {
  //Alert, Form,
  Button,
  //Row, Col, Container
} from 'react-bootstrap';
import util from 'util'

import { messages } from './employeeSearchResultsComponent.I18n'

const EmployeeSearchResultsComponent = (props) => {

  const [t2ChangeSortField, t2GetSortFunc, t2IndicatorIcon] = UseTableSorter('default', 'id', 'num')

  const backButtonClicked = () => props.clearForm()

  return (
    <IntlProvider locale={props.locale} messages={messages[props.locale]}>

      <WindowContainer caption={<FormattedMessage id="caption" />}>

        <StyledTable>
          <thead>
            <tr>
              <th onClick={() => t2ChangeSortField('id', 'num')}><FormattedMessage id="id" />{t2IndicatorIcon('id')}</th>
              <th onClick={() => t2ChangeSortField('name', 'string')}><FormattedMessage id="name" />{t2IndicatorIcon('name')}</th>
              <th onClick={() => t2ChangeSortField('age', 'num')}><FormattedMessage id="age" />{t2IndicatorIcon('age')}</th>
              <th onClick={() => t2ChangeSortField('income', 'num')}><FormattedMessage id="income" />{t2IndicatorIcon('income')}</th>
              <th onClick={() => t2ChangeSortField('born', 'datetime')}><FormattedMessage id="born" />{t2IndicatorIcon('born')}</th>
            </tr>
          </thead>

          <tbody>
            {[...props.data].sort(t2GetSortFunc().fn).map((row, i) =>
              <tr key={i} onClick={() => props.setCurrent(row)}>
                <td>{row.id}</td>
                <td>{row.name}</td>
                <td>{row.age}</td>
                <td>{row.income}</td>
                <td>{row.born}</td>
              </tr>
            )}
          </tbody>
        </StyledTable>    
        
        <br/>
        <Button onClick={() => backButtonClicked()} variant="primary" >
          {<FormattedMessage id="backBut" />}
        </Button>&nbsp;

      </WindowContainer>

    </IntlProvider>
  )
}

export default EmployeeSearchResultsComponent