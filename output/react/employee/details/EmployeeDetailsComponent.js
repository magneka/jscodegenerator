import React, { Fragment } from 'react';

import WindowContainer from '../../../common/layoutComponents/widgets/WindowContainer';
import { IntlProvider, FormattedMessage } from 'react-intl';
import styled from 'styled-components'

import {
  Alert, Form,
  Button,
  Row, Col, Container
} from 'react-bootstrap';

import { messages } from './employeeDetailsComponent.i18n'

const EmployeeDetailsComponent = (props) => {

  const backButtonClicked = () => props.setCurrent('')
  
  return (    
      <IntlProvider locale={props.locale} messages={messages[props.locale]}>
        <Fragment>
          <WindowContainer caption={<FormattedMessage id="caption" />}>       

          {/* Primary key field */} 
            
          <UcContainer>
            <UcRow>
              <UcCol className='col-md-6'>
                <UcFormGroup
                  name='id'
                  label={<FormattedMessage id="id" />}                                    
                >

                  <input
                    className="form-control col-md-2"
                    id='id'
                    name="id"
                    readOnly='readonly'
                    value={props.data.id}
                  />
                </UcFormGroup>            
              </UcCol>
            </UcRow>
          </UcContainer>
          

          {/* All the non Primary key fields */}    
            
          <UcContainer>
            <UcRow>
              <UcCol className='col-md-6'>
                <UcFormGroup
                  name='name'
                  label={<FormattedMessage id="name" />}                
                >

                  <input
                    className="form-control"
                    id='name'
                    name="name"
                    readOnly='readonly'
                    value={props.data.name}
                  />
                </UcFormGroup>             
              </UcCol>
            </UcRow>
          </UcContainer>
            
          <UcContainer>
            <UcRow>
              <UcCol className='col-md-6'>
                <UcFormGroup
                  name='age'
                  label={<FormattedMessage id="age" />}                
                >

                  <input
                    className="form-control"
                    id='age'
                    name="age"
                    readOnly='readonly'
                    value={props.data.age}
                  />
                </UcFormGroup>             
              </UcCol>
            </UcRow>
          </UcContainer>
            
          <UcContainer>
            <UcRow>
              <UcCol className='col-md-6'>
                <UcFormGroup
                  name='income'
                  label={<FormattedMessage id="income" />}                
                >

                  <input
                    className="form-control"
                    id='income'
                    name="income"
                    readOnly='readonly'
                    value={props.data.income}
                  />
                </UcFormGroup>             
              </UcCol>
            </UcRow>
          </UcContainer>
            
          <UcContainer>
            <UcRow>
              <UcCol className='col-md-6'>
                <UcFormGroup
                  name='born'
                  label={<FormattedMessage id="born" />}                
                >

                  <input
                    className="form-control"
                    id='born'
                    name="born"
                    readOnly='readonly'
                    value={props.data.born}
                  />
                </UcFormGroup>             
              </UcCol>
            </UcRow>
          </UcContainer>
          
          
          <Button onClick={() => backButtonClicked()} variant="primary" >
            {<FormattedMessage id="backBut" />}
          </Button>&nbsp;

          </WindowContainer>
        </Fragment>
      </IntlProvider>
  )
}

export default EmployeeDetailsComponent


const UcFormError = (props) => {
  return (
    <Alert variant='danger'><Alert.Heading>{props.header}</Alert.Heading>{props.message}</Alert>
  )
}

const UcFormGroup = (props) => {
  return (
    <Form.Group controlId={props.name}>
      <Form.Label>{props.label}</Form.Label>
      {props.children}      
    </Form.Group>
  )
}

const UcContainer = styled(Container)`
    margin-left: 0px;
    padding-left: 15px;
`

const UcRow = styled(Row)`    
    padding-left: 0px;
`

const UcCol = styled(Col)`    
    padding-left: 0px;
`

