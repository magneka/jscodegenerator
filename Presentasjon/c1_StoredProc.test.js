const templFuncs = require('../src/templateFunctions.js')

// I dette eksemplet skal vi lage en stored procedure for CRUD operasjoner
// Noen enterprise organisasjoner kjører alt gjennom slike for at DBA
// skal kunne kvalitetssikre all SQL

const template = `
ALTER PROCEDURE <#Tablename_UA>_UC_CRUD_PROC (
<#flds datatype=[any]>    <#KOMMA>@<#fieldname_LA> <#sqltype><#LF></#flds> 
    @StatementType NVARCHAR(20) = ''
)  
AS  
  BEGIN  
      IF @StatementType = 'Insert'  
        BEGIN  
            INSERT INTO <#Tablename_UA>  
                        (
<#flds datatype=[any]>                        <#KOMMA>@<#fieldname_LA><#LF></#flds>
                        )  
            VALUES      ( 
<#flds datatype=[any]>                        <#KOMMA>@<#fieldname_LA><#LF></#flds>            
                        )  
        END  
  
      IF @StatementType = 'Select'  
        BEGIN  
            SELECT *  
            FROM   <#Tablename_UA>  
        END  
  
      IF @StatementType = 'Update'  
        BEGIN  
            UPDATE <#Tablename_UA>  
            SET
<#flds datatype=[any] isNotPk>                   <#KOMMA><#fieldname_LA> = @<#fieldname_LA><#LF></#flds>                
            WHERE  <#flds datatype=[any] isPk><#KOMMA><#fieldname_LA> = @<#fieldname_LA><#LF></#flds>
        END  
      ELSE IF @StatementType = 'Delete'  
        BEGIN  
            DELETE FROM <#Tablename_UA>  
            WHERE  id = @id  
        END  
  END   
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true, sqltype: 'BIGINT' },
        { 'fieldname': 'Firstname', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
        { 'fieldname': 'LastName', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
        { 'fieldname': 'Born', 'datatype': 'DateTime', sqltype: 'DATE' },
        { 'fieldname': 'AnnualSales', 'datatype': 'Decimal', sqltype: 'FLOAT' },
        { 'fieldname': 'Active', 'datatype': 'Boolean', sqltype: 'VARCHAR(5)' },
        { 'fieldname': 'LastName', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
    ]
}

test('Stored procedure with crud', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})

