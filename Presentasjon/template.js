const templFuncs = require('../src/templateFunctions.js')

const template = `
CREATE TABLE <#FileName_UF>
<#flds datatype=[any] isPk>   [<#FieldName>] <#SQLtype> PRIMARY KEY <#NotNull><#flds/>
<#flds datatype=[any] isNotPk>  ,[<#FieldName>] <#SQLtype> <#NotNull><#LF><#flds/>)`

let metaData = {
    'til': 'employee',
    'nameSpace': 'mka.com',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'SQLtype': 'INTEGER', 'isId': true, 'isNullable': false, 'NotNull': 'NOT NULL' },
        { 'fieldname': 'Name', 'datatype': 'String', 'SQLtype': 'VARCHAR(80)', 'isId': false, 'NotNull': 'NOT NULL' },
        { 'fieldname': 'Age', 'datatype': 'Integer', 'SQLtype': 'SMALLINT', 'isId': false, 'NotNull': '' },
        { 'fieldname': 'Income', 'datatype': 'Decimal', 'SQLtype': 'DECIMAL(10,2)', 'isId': false, 'NotNull': '' },
        { 'fieldname': 'Born', 'datatype': 'DateTime', 'SQLtype': 'DATETIME', 'isId': false, 'NotNull': '' }
    ]
}

test('Resolve template - Create SQL', () => { console.log(templFuncs.resolveAll(metaData, template)) })
