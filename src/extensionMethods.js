// Extends String class with a new method replaceAll
if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function (strReplace, strWith) {
        var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
        var reg = new RegExp(esc, 'ig')
        return this.replace(reg, strWith)
    }
}