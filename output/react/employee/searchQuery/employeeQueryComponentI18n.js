export const messages = {
  en: {
    caption: `Search employees`,
    id: `Enter id to lookup`,
    idInfo: `You can not do wildcard search on ids, enter a complete id to see details`,    
    searchIdBut: `Load employee for Id`,    
  },
  nb: {
    caption: `Søk etter employee`, 
    id: `Oppgi id for å hente employee`,
    idInfo: `Du må oppgi full id, du kan ikke bruke jokertegn i dette feltet`,    
    searchIdBut: `Se detaljer for Id`,
  }
}