export const messages = {
  en: {
    caption: `Details for search`,
    backBut: `Go Back`,
    id: `Id`,
    name: `Name`,
    age: `Age`,
    income: `Income`,
    born: `Born`,
    
  },
  nb: {
    caption: `Detaljer`,  
    backBut: `Tilbake`,
    id: `Id`,
    name: `Name`,
    age: `Age`,
    income: `Income`,
    born: `Born`,

  }
}

