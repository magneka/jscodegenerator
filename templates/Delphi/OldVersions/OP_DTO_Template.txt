﻿unit u<#FileName_UF>;

interface

uses
  SysUtils, DBXJSON, DBXJsonReflect;

type

  I<#FileName_UF> = interface
  ['{<#GUID>}']
      function ToString(): string;   
  end;

  { Class T<#FileName_UF>, Data transfer for data from table <#FileName_LF> }
  T<#FileName_LF> = class (TInterfacedObject, I<#FileName_UF>)
  private
    // Helper var to display if data has changed since retrieved from db 
    FIsDirty: Boolean;
    
    // Helper var to use i selection from grid is needed
    FIsSelected: Boolean;

    // Helper objects, can contain anything
    FHelperObj1: TObject;
    FHelperObj2: TObject;

<#FIELDS><#IsId=true>    // Table <#FileName_UF> has unique index <#FieldName_UF>, autoincremented on server
    F<#FieldName_UF>: <#OpType>;<#IsId=true/><#FIELDS/>
<#FIELDS><#IsId=false>    F<#FieldName_UF>: <#OpType>;<#LF><#IsId=false/><#FIELDS/>

<#FIELDS><#IsId=true>    procedure Set<#FieldName_UF> (p<#FieldName_UF>: <#OpType>);<#LF><#IsId=true/><#FIELDS/>
<#FIELDS><#IsId=false>    procedure Set<#FieldName_UF> (p<#FieldName_UF>: <#OpType>);<#LF><#IsId=false/><#FIELDS/>

  public
    property IsDirty: Boolean read FIsDirty write FIsDirty;
    property IsSelected: Boolean read FIsSelected write FIsSelected;
    property HelperObj1: TObject read FHelperObj1 write FHelperObj1;
    property HelperObj2: TObject read FHelperObj2 write FHelperObj2;

<#FIELDS><#IsId=true>    property <#FieldName_UF>: <#OpType> read F<#FieldName_UF> write Set<#FieldName_UF>;<#IsId=true/><#FIELDS/>
<#FIELDS><#IsId=false>    property <#FieldName_UF>: <#OpType> read F<#FieldName_UF> write Set<#FieldName_UF>;<#LF><#IsId=false/><#FIELDS/>

    function ToJson(): string;
    function ToString(): string;
    Constructor Create(); Overload;
    function Clone(): T<#FileName_UF>;
    class function CreateFromJson(Value: string): T<#FileName_UF>;

  end;

implementation

constructor T<#FileName_UF>.Create;
begin
    Inherited;
    IsDirty := false;
    FIsSelected := false;
    FHelperObj1 := nil;
    FHelperObj2 := nil;
end;

function T<#FileName_UF>.Clone(): T<#FileName_LF>;
begin
    
   result :=  T<#FileName_LF>.Create();

<#FIELDS><#IsId=true>   result.<#FieldName_UF> := self.<#FieldName_UF>;<#LF><#IsId=true/><#FIELDS/>
<#FIELDS><#IsId=false>   result.<#FieldName_UF> := self.<#FieldName_UF>;<#LF><#IsId=false/><#FIELDS/>

   IsDirty := false;
end;

class function T<#FileName_UF>.CreateFromJson(Value: string): T<#FileName_LF>;
const
  JsonWrapper =  '{"type":"u<#FileName_UF>.T<#FileName_UF>","id":1,"fields":%s}';

var
  lUnMarshal: TJSONUnMarshal;
  lJSONValue: TJSONValue;

begin
  lUnMarshal := TJSONUnMarshal.Create();
  try
    Try
       lJSONValue := TJSONObject.ParseJSONValue(TEncoding.Default.GetBytes(Format (JsonWrapper, [Value])), 0);
       Result := lUnMarshal.Unmarshal(lJSONValue) as T<#FileName_LF>;
    except
       on E: Exception do
       begin
          lJSONValue := TJSONObject.ParseJSONValue(TEncoding.Default.GetBytes(Value), 0);
          Result := lUnMarshal.Unmarshal(lJSONValue) as T<#FileName_LF>;
       end;
    End;
  finally
    FreeAndNil(lUnMarshal);
  end;
end;


function T<#FileName_UF>.ToJson: string;
var
  lMarshal: TJSONMarshal;

begin
  lMarshal := TJSONMarshal.Create(TJSONConverter.Create);
  try
    Result := lMarshal.Marshal(Self).ToString();
  finally
    FreeAndNil(lMarshal);
  end;

end;

<#FIELDS> <#IsId=true>procedure T<#FileName_UF>.Set<#FieldName_UF> (p<#FieldName_UF>: <#OpType>);
begin
   if p<#FieldName_UF> <> F<#FieldName_UF> then
   begin
      F<#FieldName_UF> := p<#FieldName_UF>;
      IsDirty := true;
   end;
end;

<#IsId=true/><#FIELDS/>

<#FIELDS><#IsId=false>procedure T<#FileName_UF>.Set<#FieldName_UF> (p<#FieldName_UF>: <#OpType>);
begin
   if p<#FieldName_UF> <> F<#FieldName_UF> then
   begin
      F<#FieldName_UF> := p<#FieldName_UF>;
      IsDirty := true;
   end;
end;


<#IsId=false/><#FIELDS/>


function T<#FileName_UF>.ToString(): String;
begin
      Result := 'T<#FileName_LF>'
<#FIELDS>
<#string>	            + ', <#FieldName_UF>: ' + F<#FieldName_UF><#LF><#string/>
<#int>	            + ', <#FieldName_UF>: ' + IntToStr(F<#FieldName_UF>)<#LF><#int/>
<#int?>	            + ', <#FieldName_UF>: ' + IntToStr(F<#FieldName_UF>)<#LF><#int?/>
<#decimal?>	            + ', <#FieldName_UF>: ' + CurrToStr(F<#FieldName_UF>)<#LF><#decimal?/>
<#float?>	            + ', <#FieldName_UF>: ' + CurrToStr(F<#FieldName_UF>)<#LF><#float?/>
<#datetime?>	            + ', <#FieldName_UF>: ' + DateToStr(F<#FieldName_UF>)<#LF><#datetime?/>
<#Boolean?>	            + ', <#FieldName_UF>: ' + BoolToStr(F<#FieldName_UF>)<#LF><#Boolean?/>
<#FIELDS/>	            ;

end;

end.