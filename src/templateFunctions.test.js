
const templFuncs = require('./templateFunctions.js')

describe('Template Core functions', () => {

    const exampleTable = {
        'fileName': 'employee',
        'fields': [
            { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true, 'isNullable': false },
            { 'fieldname': 'Name', 'datatype': 'String' },
            { 'fieldname': 'Age', 'datatype': 'Integer' },
            { 'fieldname': 'Born', 'datatype': 'DateTime' },
        ]
    }

    beforeAll(() => {       
    });


    beforeEach(() => {
        //initializeCityDatabase();
    });

    afterEach(() => {
        //clearCityDatabase();
    });
    
    test('Find outer template ending start point', () => {
        //                                     01234567890123456789
        const testres = templFuncs.lastPosBeg('<tag><tag>abc</tag></tag>', '</tag>')
        expect(testres).toBe(19);
    });

    test('Find outer template ending end point', () => {
        //                                     01234567890123456789
        const testres = templFuncs.firstPosEnd('<tag><tag>abc</tag></tag>', '<tag>')
        expect(testres).toBe(5);
    });

    test('Dont Find outer template ending start point', () => {
        //                                     01234567890123456789
        const testres = templFuncs.lastPosBeg('<tag><tag>abc</tag></tag>', '</tagg>')        
        expect(testres).toBe(-1);
    });

    test('Dont Find outer template ending end point', () => {
        const testres = templFuncs.firstPosEnd('<tag><tag>abc</tag></tag>', '<tagg>')
        //                                     01234567890123456789        
        expect(testres).toBe(-1);
    });

    test('Capitalize', () => {
        const testres = templFuncs.capitalize('magne')  
        expect(testres).toBe('Magne');
    });
    
    test('Decapitalize', () => {
        const testres = templFuncs.deCapitalize('Magne')
        expect(testres).toBe('magne');
    });

    // ReplaceVariants
    test('Test <#FieldName_UF>', () => {
        const testres = templFuncs.replaceVariants('the fieldName is <#FieldName_UF>', 'FieldName', 'firstname' )
        expect(testres).toBe('the fieldName is Firstname');
    });

    test('Test <#FieldName_LF>', () => {
        const testres = templFuncs.replaceVariants('the fieldName is <#FieldName_LF>', 'FieldName', 'FirstName')
        expect(testres).toBe('the fieldName is firstName');
    });

    test('Test <#FieldName_UA>', () => {
        const testres = templFuncs.replaceVariants('the fieldName is <#FieldName_UA>', 'FieldName', 'firstname')
        expect(testres).toBe('the fieldName is FIRSTNAME');
    });

    test('Test <#FieldName_LA>', () => {
        const testres = templFuncs.replaceVariants('the fieldName is <#FieldName_LA>', 'FieldName', 'FirstName')
        expect(testres).toBe('the fieldName is firstname');
    });

    test('Replace Linefeeds', () => {
        const testres = templFuncs.fixTemplateTags('firstline<#LF>secondline')        
        expect(testres).toContain('\n');
    });

    test('Replace Tabs', () => {
        const testres = templFuncs.fixTemplateTags('firstline<#TAB>secondline')
        expect(testres).toContain('\t');
    });

    test('Get Inner Start and End', () => {
        const template = "xxx<#flds datatype=[any]> what goes on here <#FieldName_UF > <#LF><#flds/>xxx"
        //                01234567890123456789012345678901234567890123456789012345678901234567890123456789
        // lookfor           ^                                                                      ^
        const testres = templFuncs.getInnerStartEnd(template, 'flds')
        expect(testres[0]).toBe(3);
        expect(testres[1]).toBe(74);
    });

    // Finn ut alt foran, bak og informasjon om hva som skal resolves (datatype)
    test('Get splitted inner section', () => {
        const template = "<flds>xxx<#flds datatype=[any]>what goes on here<#flds/>xxx<#flds/>"
        const testres = templFuncs.getSplitInnerSection(template, 'flds')
        //console.log(testres)
        expect(testres[0]).toBe('<flds>xxx');
        expect(testres[1]).toBe('datatype=[any]');
        expect(testres[2]).toBe('what goes on here');
        expect(testres[3]).toBe('xxx<#flds/>');
    });

    test('Get datatypes single', () => {        
        const testres = templFuncs.getDataTypes('datatype=[any]', 'datatype')
        //console.log(testres)
        expect(testres[0]).toBe('any');        
    });

    test('Get datatypes two types', () => {        
        const testres = templFuncs.getDataTypes('datatype=[string, integer]','datatype')
        //console.log(testres)
        expect(testres[0]).toBe('string');
        expect(testres[1]).toBe('integer');        
    });

    test('Get constraints IsId', () => {
        const testres = templFuncs.getConstraints('datatype=[string, integer] isId')
        //console.log(testres)
        //{ IsPk: false, IsNotPk: false, IsNullable: false }
        expect(testres.IsPk).toBe(true);
        expect(testres.IsNotPk).toBe(false);
        expect(testres.IsNullable).toBe(false);
    });

    test('Get constraints IsPk', () => {
        const testres = templFuncs.getConstraints('datatype=[string, integer] isPk')
        //console.log(testres)

        expect(testres.IsPk).toBe(true);
        expect(testres.IsNotPk).toBe(false);
        expect(testres.IsNullable).toBe(false);
    });

    test('Get constraints IsNotPk', () => {
        const testres = templFuncs.getConstraints('datatype=[string, integer] isNotPk')
        //console.log(testres)
        //{ IsPk: false, IsNotPk: false, IsNullable: false }
        expect(testres.IsPk).toBe(false);
        expect(testres.IsNotPk).toBe(true);
        expect(testres.IsNullable).toBe(false);
    });

    test('Get constraints IsNullable', () => {
        const testres = templFuncs.getConstraints('datatype=[string, integer] isNullable')
        //console.log(testres)
        //{ IsPk: false, IsNotPk: false, IsNullable: false }
        expect(testres.IsPk).toBe(false);
        expect(testres.IsNotPk).toBe(false);
        expect(testres.IsNullable).toBe(true);
    });

    test('Resolve simple template - Any', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, '<#KOMMA><#FieldName_UF>', ['any'], [],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        expect(testres).toBe('Id, Name, Age, Born')   
    });

    test('Resolve simple template - Integer', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, '<#KOMMA><#FieldName_UF>', ['Integer'], [],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        expect(testres).toBe('Id, Age')
    });

    test('Resolve simple template - String og datetime', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, '<#KOMMA><#FieldName_UF>', ['String', 'DateTime'], [],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        expect(testres).toBe('Name, Born')
    });

    test('Resolve simple template - String og datetime', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, '<#KOMMA><#FieldName_UF>', ['String', 'DateTime'], [],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        expect(testres).toBe('Name, Born')
    });

    test('Resolve simple template - exclude String og datetime', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, '<#KOMMA><#FieldName_UF>', [], ['String', 'DateTime'],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        expect(testres).toBe('Id, Age')
    });

    test('Resolve one simple template - C# Getters And setters', () => {
        const testres = templFuncs.resolveOneTemplate(
            exampleTable, 'public <#Datatype> <#FieldName_UF> {get; set;}<#LF>', ['String', 'DateTime'], [],
            { IsPk: false, IsNotPk: false, IsNullable: false })
        //console.log(testres)
        
        let testResAsArr = testres.split('\n')
        expect(testResAsArr[0]).toBe('public String Name {get; set;}')
        expect(testResAsArr[1]).toBe('public DateTime Born {get; set;}')
    });
    //resolvetags
    test('Resolve template - C# Getters And setters - Any', () => {
        const template = `<#flds datatype=[any]>public <#Datatype> <#FieldName_UF> {get; set;}<#LF><#flds/>`
        const testres = templFuncs.resolveAll(exampleTable, template)
        //console.log(testres)

        let testResAsArr = testres.split('\n')
        expect(testResAsArr[0]).toBe('public Integer Id {get; set;}')
        expect(testResAsArr[1]).toBe('public String Name {get; set;}')
        expect(testResAsArr[2]).toBe('public Integer Age {get; set;}')
        expect(testResAsArr[3]).toBe('public DateTime Born {get; set;}')
    });

    test('Resolve template - C# Method declaration', () => {
        const template = `public SomeMethod (<#flds datatype=[any]><#KOMMA><#Datatype> <#FieldName_UF><#flds/>){}`
        const testres = templFuncs.resolveAll(exampleTable, template)
        console.log(testres)

        expect(testres).toBe('public SomeMethod (Integer Id, String Name, Integer Age, DateTime Born){}');
    });

    test('Resolve template - Create SQL', () => {

        let tableDef = {
            'fileName': 'employee',
            'nameSpace': 'mka.com',
            'fields': [
                { 'fieldname': 'Id', 'datatype': 'Integer', 'SQLtype': 'INTEGER', 'isId': true, 'isNullable': false, 'NotNull': 'NOT NULL' },
                { 'fieldname': 'Name', 'datatype': 'String', 'SQLtype': 'VARCHAR(80)', 'isId': false, 'NotNull': 'NOT NULL' },
                { 'fieldname': 'Age', 'datatype': 'Integer', 'SQLtype': 'SMALLINT', 'isId': false, 'NotNull': '' },
                { 'fieldname': 'Income', 'datatype': 'Decimal', 'SQLtype': 'DECIMAL(10,2)', 'isId': false, 'NotNull': '' },
                { 'fieldname': 'Born', 'datatype': 'DateTime', 'SQLtype': 'DATETIME', 'isId': false, 'NotNull': '' }
            ]
        }        
        const template = `
CREATE TABLE <#FileName_UF>
<#flds datatype=[any] isPk>   [<#FieldName>] <#SQLtype> PRIMARY KEY <#NotNull><#flds/>
<#flds datatype=[any] isNotPk>  ,[<#FieldName>] <#SQLtype> <#NotNull><#LF><#flds/>)`
        
        const testRes = templFuncs.resolveAll(tableDef, template).split('\n')
        
        expect(testRes[1]).toBe('CREATE TABLE Employee')
        expect(testRes[2]).toBe('   [Id] INTEGER PRIMARY KEY NOT NULL')
        expect(testRes[3]).toBe('  ,[Name] VARCHAR(80) NOT NULL')
        expect(testRes[4]).toBe('  ,[Age] SMALLINT ')
        expect(testRes[5]).toBe('  ,[Income] DECIMAL(10,2) ')
        expect(testRes[6]).toBe('  ,[Born] DATETIME ')
        expect(testRes[7]).toBe(')')

    });
})
