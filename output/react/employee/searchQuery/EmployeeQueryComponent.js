import React, { Fragment, useState } from 'react';

import WindowContainer from '../../../common/layoutComponents/widgets/WindowContainer';
import { IntlProvider, FormattedMessage } from 'react-intl';
import { useIntl } from 'react-intl';


import {
  Alert, Form,
  Button,
  Row, Col, Container
} from 'react-bootstrap';
import styled from 'styled-components'

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { messages } from './employeeSearchQueryComponent.i18n'


const EmployeeSearchQueryComponent = (props) => {

  const { formatMessage: f } = useIntl();

  const searchForId = (OkMessage, NotFoundMessage, restApiError) => {
    props.search(
      searchId,
      '',
      f({ id: 'okmessage' }),
      f({ id: 'notfoundmessage' }),
      f({ id: 'restapierror' })
    )
  }

  const searchForName = (OkMessage, NotFoundMessage, restApiError) => {
    props.search('',
      searchName,
      f({ id: 'okmessage' }),
      f({ id: 'notfoundmessage' }),
      f({ id: 'restapierror' })
    )
  }

  const [searchName, setSearchName] = useState('')
  const [searchId, setSearchId] = useState('')

  return (
    <IntlProvider locale={props.locale} messages={messages[props.locale]}>
      <Fragment>
        <WindowContainer caption={<FormattedMessage id="caption" />}>

          <ToastContainer autoClose={4000} />

          <UcFormGroup
            name='id'
            label={<FormattedMessage id="id" />}
            errors=''
            information={<FormattedMessage id="idInfo" />}
            errorMessage={<FormattedMessage id="idError" />}
          >
            <Input
              className="form-control col-md-2"
              id='id'
              name="id"
              type="number"
              min="0"
              step="0"
              onChange={e => setSearchId(e.target.value)}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  searchForId()
                }
              }}
            />
          </UcFormGroup>

          <UcFormGroup
            name='name'
            label={<FormattedMessage id="name" />}
            errors=''
            information={<FormattedMessage id="nameInfo" />}
            errorMessage={<FormattedMessage id="firstNameError" />}
          >

            <input
              className="form-control"
              id='firstName'
              name="firstName"
              data-testid="firstName"
              onChange={e => setSearchName(e.target.value)}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  searchForName()
                }
              }}
            />
          </UcFormGroup>


          <Button onClick={() => searchForId()} variant="primary" disabled={props.state.isLoading || searchId === ''}>
            {<FormattedMessage id="searchIdBut" />}
          </Button>&nbsp;

          <Button onClick={() => searchForName()} variant="primary" disabled={props.state.isLoading || searchName === ''}>
            {<FormattedMessage id="searchNameBut" />}
          </Button>&nbsp;


        </WindowContainer>
      </Fragment>
    </IntlProvider>
  )
}

export default EmployeeSearchQueryComponent


const UcFormGroup = (props) => {
  return (
    <Form.Group controlId={props.name}>
      <Form.Label>{props.label}</Form.Label>
      {props.children}
      {props.information && <Form.Text className="text-muted">{props.information}</Form.Text>}
      {props.errors[props.name] && <span data-testid={props.name + '_err'} style={{ color: 'red' }}>{props.errorMessage}</span>}
    </Form.Group>
  )
}

const UcContainer = styled(Container)`
  margin-left: 0px;
  padding-left: 15px;
`

const UcRow = styled(Row)`    
  padding-left: 0px;
`

const UcCol = styled(Col)`    
  padding-left: 0px;
`

const Input = styled.input`
  ::-webkit-inner-spin-button{
      -webkit-appearance: none; 
      margin: 0; 
  }
  ::-webkit-outer-spin-button{
      -webkit-appearance: none; 
      margin: 0; 
  }    
`;

