const templFuncs = require('../src/templateFunctions.js')

// I dette eksemplet skal vi lage en plain controller med CRUD operasjoner

const template = `
using System.Collections.Generic;
using DemoServer.Source.Datalayer.Repositories.<#Tablename_UF>;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace FunctionalRepo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class <#Tablename_UF>Controller : ControllerBase
    {
        private readonly ILogger<<#Tablename_UF>Controller> _logger;
        public I<#Tablename_UF>Repository _<#Tablename_UF>Repository { get; set; }

        public <#Tablename_UF>Controller(ILogger<<#Tablename_UF>Controller> logger, I<#Tablename_UF>Repository <#Tablename_LF>Repository)
        {
            _logger = logger;
            _<#Tablename_UF>Repository = <#Tablename_LF>Repository;
        }

        [HttpGet]
        public IEnumerable<<#Tablename_UF>Dto> Get()
        {
            var result = _<#Tablename_UF>Repository.GetAll();
            _logger.LogInformation($"Retrieved {result.Count} <#Tablename_LF>s");           
            return result;
        }

        // GET: api/<#Tablename_UF>/5
        [HttpGet("{<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>}")]
        public ActionResult<<#Tablename_UF>Dto> GetById(<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>)
        {
            var result = _<#Tablename_UF>Repository.GetById(<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>);
            _logger.LogInformation($"Retrieved <#Tablename_LF># {<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>} : {JsonSerializer.Serialize(result)}.");     

            if (result != null) 
                return result;
            
            return NotFound();
        }

        // GET: api/<#Tablename_UF>/Kar
        [HttpGet("Search/{name}")]
        public IEnumerable<<#Tablename_UF>Dto> SearchByName(string name)
        {
            var result = _<#Tablename_UF>Repository.Search("Name", name);
            _logger.LogInformation($"SearchByName <#Tablename_LF># {name} : Retrieved {result.Count} <#Tablename_LF>s.");     
            
            return result;
        }

        // PUT: api/<#Tablename_UF>
        [HttpPut("{<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>}")]
        public IActionResult Update(<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>, <#Tablename_UF>Dto <#Tablename_LF>)
        {
            var result = _<#Tablename_UF>Repository.Update(<#Tablename_LF>);
            _logger.LogInformation($"Update <#Tablename_LF># {<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>} : {JsonSerializer.Serialize(<#Tablename_LF>)}, result: {result} ."); 

            if (result != 1) 
                return BadRequest();
            
            return Ok(result);    
        }

        // POST: api/<#Tablename_UF>
        [HttpPost]
        public ActionResult<<#Tablename_UF>Dto> PostNew([FromBody] <#Tablename_UF>Dto <#Tablename_LF>)
        {
            var result = _<#Tablename_UF>Repository.Add(<#Tablename_LF>);
            _logger.LogInformation($"Add <#Tablename_LF># : {JsonSerializer.Serialize(<#Tablename_LF>)}, result: {JsonSerializer.Serialize(result)}."); 
            
            if (result.Id > 0) 
                return Ok(result);    
            
            return BadRequest();                        
        }

        // DELETE: api/<#Tablename_UF>/5
        [HttpDelete("{<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>}")]
        public ActionResult<int> DeleteById(<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>)
        {
            var result = _<#Tablename_UF>Repository.Delete(<#flds datatype=[any] isPk><#datatype> <#fieldname_LF></#flds>);
            
            if (result == 1) 
                return Ok(result);    
            
            return BadRequest();    
        }      
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'Customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true, sqltype: 'BIGINT' },
        { 'fieldname': 'Firstname', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
        { 'fieldname': 'LastName', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
        { 'fieldname': 'Born', 'datatype': 'DateTime', sqltype: 'DATE' },
        { 'fieldname': 'AnnualSales', 'datatype': 'Decimal', sqltype: 'FLOAT' },
        { 'fieldname': 'Active', 'datatype': 'Boolean', sqltype: 'VARCHAR(5)' },
        { 'fieldname': 'LastName', 'datatype': 'String', sqltype: 'VARCHAR(30)' },
    ]
}

test('Controller with crud operations', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})
