const templFuncs = require('../src/templateFunctions.js')

/*
I dette eksemplet skal vi lage en javastyle toString metode.
Problemstillingen er at dersom datatypen ikke er av string, må vi kalle .ToString()
på property.  Da trenger vi å behandle noen datatyper annerledes:
Vi kan bruke nkludering og ekskludering av datatyper i felter:
  <#flds datatype=[Integer]>....</#flds>
  <#flds excluded=[Integer]>....</#flds>
*/

const template = `
using system;
using System.ComponentModel.DataAnnotations;

namespace <#Namespace>.<#Tablename>
{
    public class <#Tablename>Dto
    {
<#flds datatype=[any] isPk>
        [Key]
        public <#datatype> <#fieldname> {get; set;}<#LF>
<#flds/>
<#flds datatype=[any] isNotPk>        public <#datatype> <#fieldname> {get; set;}<#LF></#flds>

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

<#flds datatype=[Integer]>            if (<#fieldname_UF> != null)
                result.Append("<#fieldname_UF>:").AppendLine(<#fieldname_UF>);<#LF></#flds>

<#flds excluded=[Integer]>            if (<#fieldname_UF> != null)
                result.Append("<#fieldname_UF>:").AppendLine(<#fieldname_UF>);<#LF></#flds>                          
            return result.ToString();
        }
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'Customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true },
        { 'fieldname': 'Firstname', 'datatype': 'String' },
        { 'fieldname': 'LastName', 'datatype': 'String' }
    ]
}

test('CSharp javastyle tostring method', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})

