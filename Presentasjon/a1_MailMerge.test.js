const templFuncs = require('../src/templateFunctions.js')

/* 
Dette er et eksempel på hvordan en mail merge fungerer,
vi har metadata som blir erstattet i templaten.
*/
let metaData = {
    'fornavn': 'TORODD',
    'etternavn': 'Tomte Knudsen',
    'emne': 'Automatisk generering av KODE',
    'avsender': 'MAGNE'
}

const template = `
TIl:         <#fornavn> <#etternavn>
Vedrørende:  <#emne>

Du er hjertelig velkommen til møte om <#emne> som du utfordret meg på.

hilsen
<#avsender>`

test('Mailmerge', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})

