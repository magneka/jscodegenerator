/* eslint-disable no-useless-escape */
// eslint-disable-next-line no-extend-native
String.prototype.replaceAll = function (strReplace, strWith) {
  // See http://stackoverflow.com/a/3561711/556609
  var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
  var reg = new RegExp(esc, 'ig')
  return this.replace(reg, strWith)
}

exports.generate = (tableInfo, sourceCodeTemplate) => {
  const FLDS = 'flds'
  const COMMA = `<#KOMMA>`
  const LF = `<#LF>`
  const TAB = `<#TAB>`
  // const ISPK = 'IsPk'
  // const ISNULLABLE = 'IsNullable'

  const lastPosBeg = (s, t) => s.lastIndexOf(t)
  const firstPosEnd = (s, t) => s.indexOf(t) + t.length
  const startTag = t => `<#${t}`
  const endTag = t => `<#${t}/>`
  const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  const deCapitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toLowerCase() + s.slice(1)
  }

  const replaceVariants = (s, key, val) => {
    return s.replaceAll(`<#${key}>`, val)
      .replaceAll(`<#${key}_UF>`, capitalize(val))
      .replaceAll(`<#${key}_LF>`, deCapitalize(val))
      .replaceAll(`<#${key}_UA>`, val.toUpperCase())
      .replaceAll(`<#${key}_LA>`, val.toLowerCase())
  }

  const fixTemplateTags = s => {
    return s.replaceAll(LF, '\n').replaceAll(TAB, '\t')
  }

  const fixCompabilityIssues = s => {
    return s.replaceAll(`<#${FLDS}`, `<#${FLDS}`)
      .replaceAll(`<#${FLDS} />`, `<#${FLDS}/>`)
      .replaceAll('Datatypes = [', 'datatype=[')
      .replaceAll('DatatypesIn = [', 'datatype=[')
  }

  const getInnerStartEnd = (s, t) => {
    let ep = firstPosEnd(s, endTag(t))
    return [ lastPosBeg(s.substring(0, ep - endTag(t).length), startTag(t)), firstPosEnd(s, endTag(t)) ]
  }

  const getSplitInnerSection = (s, t) => {
    let a, b
    [a, b] = getInnerStartEnd(s, t)

    let bs = s.substring(a + startTag(t).length + 1, b - endTag(t).length)
    let i = bs.indexOf('>')

    return [s.substring(0, a), ...bs.substring(0, i).split('> '), bs.substring(i + 1, b), s.substring(b)]
  }

  const getDataTypes = (s, dtype) => {
    return s.replace('[', '')
      .replace(' ', '')
      .split(']')
      .filter((x) => x.includes('='))
      .reduce((res, item) => {
        if (item.includes('=') && (item.includes(dtype))) {
          // eslint-disable-next-line no-unused-vars
          let d, f
          [d, f] = item.split('=')
          res.push(...f.split(','))
        }
        return res
      }, [])
  }

  const getConstraints = s => {
    let result = {
      'IsPk': false,
      'IsNotPk': false,
      'IsNullable': false
    }
    if (s.includes('isId')) { result['IsPk'] = true }
    if (s.includes('isPk')) { result['IsPk'] = true }
    if (s.includes('isNotPk')) { result['IsNotPk'] = true }
    if (s.includes('isNullable')) { result['IsNullable'] = true }
    return result
  }

  // Figure out if this section is to be resolved or skipped, then resolve
  const resolveOneTemplate = (table, tp, datatypes, constraints) => {
    //console.log(resolveOneTemplate, table, tp)
    let komma = ''
    return table.fields.reduce((res, item) => {

      let skip = true

      if ((datatypes.includes(item.datatype)) || (datatypes.includes('any'))) {
        skip = false
      }

      if (item.isId === true) {
        if (constraints.IsPk === true) 
          skip = false;
      
          if (constraints.IsNotPk === true) 
            skip = true;
      }
     
      if ((constraints.isNullable === true) && (item.isNullable !== true)) {
        skip = true
      }
      if (!skip) {
        res = res + resolveTags(tp, item)
      }
      res = res.replaceAll(COMMA, komma)
      komma = ', '
      return res
    }, '')
  }

  const resolveTags = (template, item) => {
    let result = template

    Object.entries(item).forEach(([key, value]) => {
      if (typeof value === 'string' || value instanceof String) {
        // console.log(`${key}: ${value}`)
        result = replaceVariants(result, key, value)
        result = fixTemplateTags(result)
      }
    })
    return result
  }

  const resolveAll = (table, sourceCode) => {
    let result = fixCompabilityIssues(sourceCode)

    while ((result.includes(startTag(FLDS))) && (result.includes(endTag(FLDS)))) {
      let a, b, template, parms
      [a, parms, template, b] = getSplitInnerSection(result, FLDS)

      let datatypes = getDataTypes(parms, 'datatype')
      let constraints = getConstraints(parms)

      let resolvedTemplate = resolveOneTemplate(table, template, datatypes, constraints)

      result = a + resolvedTemplate + b
    }

    result = replaceVariants(result, 'FileName', table.fileName)
    result = replaceVariants(result, 'Namespace', table.nameSpace)

    return result
  }

  return resolveAll(tableInfo, sourceCodeTemplate)
}
