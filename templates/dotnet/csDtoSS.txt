using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace <#Namespace>.<#FileName_LF>
{
    public class <#FileName_UF>
    {
        public enum <#FileName_UF>Fields
        {
<#flds datatype=[any]>            <#KOMMA><#FieldName_UF><#LF><#flds />
        }       

		<#flds datatype=[any]>
        private <#Datatype> _<#FieldName_LF>;
        public <#Datatype> <#FieldName_UF> { get => _<#FieldName_LF>; set { _<#FieldName_LF> = value; IsDirty = true; } }
		<#flds />
        // Helper var to display if data has changed since retrieved from db 
        [NotMapped]
        public Boolean IsDirty { get; set; }
    
        // Helper var to use i selection from grid is needed
        [NotMapped]
        public Boolean IsSelected { get; set; }

        // Helper objects, can contain anything
        [NotMapped]
        public Object HelperObj1 { get; set; }
        
        [NotMapped]
        public Object HelperObj2 { get; set; }
                
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
<#flds datatype=[any]>
            if (<#FieldName_UF> != null)
                result.Append("<#FieldName_UF>:").AppendLine(<#FieldName_UF>.ToString());
<#flds />          
            return result.ToString();
        }
    }
}