require('./extensionMethods')
module.exports = {

  FLDS: 'flds',
  COMMA: `<#KOMMA>`,
  LF: `<#LF>`,
  TAB: `<#TAB>`,

  //exports.FLDS = FLDS
  //exports.COMMA = COMMA
  //exports.LF = LF
  //exports.TAB = TAB

  // const ISPK = 'IsPk'
  // const ISNULLABLE = 'IsNullable'

  lastPosBeg: (s, t) => s.lastIndexOf(t),
  firstPosEnd: (s, t) => {
    let res = s.indexOf(t) + t.length
    if (res == t.length - 1)
      return -1
    return res
  },
  startTag: t => `<#${t}`,
  endTag: t => `<#${t}/>`,
  capitalize: (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  },
  //exports.lastPosBeg = lastPosBeg
  //exports.firstPosEnd = firstPosEnd
  //exports.startTag = startTag
  //exports.endTag = endTag
  //exports.capitalize = capitalize

  deCapitalize: (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toLowerCase() + s.slice(1)
  },
  //exports.deCapitalize = deCapitalize

  replaceVariants: (s, key, val) => {
    //console.log(`replaceVariants s:${s}, key:${key}, val:${val}`)
    // Handle undefined
    if ((val !== '') && (!val))
      return s
    return s.replaceAll(`<#${key}>`, val)
      .replaceAll(`<#${key}_UF>`, module.exports.capitalize(val))
      .replaceAll(`<#${key}_LF>`, module.exports.deCapitalize(val))
      .replaceAll(`<#${key}_UA>`, val.toUpperCase())
      .replaceAll(`<#${key}_LA>`, val.toLowerCase())
  },
  //exports.replaceVariants

  fixTemplateTags: s => {
    return s.replaceAll(module.exports.LF, '\n').replaceAll(module.exports.TAB, '\t')
  },
  //exports.fixTemplateTags = fixTemplateTags

  fixCompabilityIssues: s => {
    return s.replaceAll(`<#${module.exports.FLDS}`, `<#${module.exports.FLDS}`)
      .replaceAll(`<#${module.exports.FLDS} />`, `<#${module.exports.FLDS}/>`)
      .replaceAll(`</#${module.exports.FLDS}>`, `<#${module.exports.FLDS}/>`)
      .replaceAll('Datatypes = [', 'datatype=[')
      .replaceAll('DatatypesIn = [', 'datatype=[')
  },
  //exports.fixCompabilityIssues = fixCompabilityIssues

  getInnerStartEnd: (s, t) => {
    //console.log(`getInnerStartEnd s:${s}, key:${t}`)
    let ep = module.exports.firstPosEnd(s, module.exports.endTag(t))
    return [module.exports.lastPosBeg(s.substring(0, ep - module.exports.endTag(t).length), module.exports.startTag(t)), module.exports.firstPosEnd(s, module.exports.endTag(t))]
  },
  //exports.getInnerStartEnd = getInnerStartEnd

  getSplitInnerSection: (s, t) => {
    let a, b
    [a, b] = module.exports.getInnerStartEnd(s, t)

    let bs = s.substring(a + module.exports.startTag(t).length + 1, b - module.exports.endTag(t).length)
    let i = bs.indexOf('>')

    return [s.substring(0, a), ...bs.substring(0, i).split('> '), bs.substring(i + 1, b), s.substring(b)]
  },
  //exports.getSplitInnerSection = getSplitInnerSection

  getDataTypes: (s, dtype) => {
    //console.log(`getDataTypes s:${s}, key:${dtype}`)
    return s.replace('[', '')
      .replace(' ', '')
      .split(']')
      .filter((x) => x.includes('='))
      .reduce((res, item) => {
        if (item.includes('=') && (item.includes(dtype))) {
          // eslint-disable-next-line no-unused-vars
          let d, f
          [d, f] = item.split('=')
          res.push(...f.split(','))
        }
        return res
      }, [])
  },
  //exports.getDataTypes = getDataTypes

  // TODO make case insensitive
  getConstraints: s => {
    //console.log(`getConstraints s:${s}`)
    let result = {
      'IsPk': false,
      'IsNotPk': false,
      'IsNullable': false
    }
    if (s.includes('isId')) { result['IsPk'] = true }
    if (s.includes('isPk')) { result['IsPk'] = true }
    if (s.includes('isNotPk')) { result['IsNotPk'] = true }
    if (s.includes('isNullable')) { result['IsNullable'] = true }
    return result
  },
  //exports.getConstraints = getConstraints

  // Figure out if this section is to be resolved or skipped, then resolve
  resolveOneTemplate: (table, tp, datatypes, excludedDatatypes, constraints) => {
    //console.log(resolveOneTemplate, table, tp)
    //console.log ('******************************************************************************')
    //console.log(`resolveOneTemplate tp:${tp}, \ndatatypes:${datatypes}, \nconstraints:`, constraints)
    const komma = ', '
    let result = table.fields.reduce((res, item) => {

      let skip = false

      /*
         LOGIC:
            1 Find out if fieldtype is included, set skip to false
            2 Check if is isPk is set exclude accordingly
            3 check if isNotPk is set, then exclude accordingly
            4 check if isNullable is set, then exclude accordingly
       */

      if (excludedDatatypes !== undefined && excludedDatatypes.length !== 0) {

        if (excludedDatatypes.includes(item.datatype))
          skip = true;
      }
      else if (datatypes !== undefined || datatypes.length !== 0) {
        
        if (!(datatypes.includes('any')) && (!datatypes.includes(item.datatype)))
          skip = true
      }

      if ((constraints.IsPk) && (item.isId !== true)) 
        skip = true
      else if ((constraints.IsNotPk) && (item.isId === true))
        skip = true

      if ((constraints.isNullable) && (item.isNullable !== true))
        skip = true
      
      if (!skip) 
        res = res + module.exports.resolveTags(tp, item)
    
      //res = res.replaceAll(module.exports.COMMA, komma)
      return res
    }, '')

    // remove initial comma, so it separatest items
    result = result.replace(module.exports.COMMA, '');
    return result.replaceAll(module.exports.COMMA, komma) 
  },
  //exports.resolveOneTemplate = resolveOneTemplate


  resolveTags: (template, item) => {
    //console.log(`resolveTags template:${template}, item:`, item)
    let result = template

    Object.entries(item).forEach(([key, value]) => {
      //console.log('1', value, (typeof value === 'string'))
      //console.log('2', value, (value instanceof String))
      //console.log('3', value, (value === ''))
      if (typeof value === 'string' || value instanceof String || value === '') {
        //console.log('4', `'${key}': '${value}'`)
        result = module.exports.replaceVariants(result, key, value)
        result = module.exports.fixTemplateTags(result)
      }
    })
    return result
  },
  //exports.resolveTags = resolveTags

  resolveAll: (table, sourceCode) => {
    let result = module.exports.fixCompabilityIssues(sourceCode)

    while ((result.includes(module.exports.startTag(module.exports.FLDS))) && (result.includes(module.exports.endTag(module.exports.FLDS)))) {
      let a, b, template, parms
      [a, parms, template, b] = module.exports.getSplitInnerSection(result, module.exports.FLDS)

      let datatypes = module.exports.getDataTypes(parms, 'datatype')
      let excludedDataTypes = module.exports.getDataTypes(parms, 'excluded')
      let constraints = module.exports.getConstraints(parms)

      let resolvedTemplate = module.exports.resolveOneTemplate(table, template, datatypes, excludedDataTypes, constraints)

      result = a + resolvedTemplate + b
    }

    for (let tag in table) {
      if (tag !== 'fields') {
        //console.log(tag, table[tag])
        result = module.exports.replaceVariants(result, tag, table[tag])    
      }
    }

    //result = module.exports.replaceVariants(result, 'FileName', table.fileName)
    //result = module.exports.replaceVariants(result, 'Namespace', table.nameSpace)

    

    return result
  }
  //exports.resolveAll = resolveAll

}
