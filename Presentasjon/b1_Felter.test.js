const templFuncs = require('../src/templateFunctions.js')

/*
Vi utvider forrige eksempel med å legge til properties
Da trenger vi å genere en kodelinje for hvert felt
Vi bruker da flds tag på denne måten for å gjøre dette for alle datatyper:
    <#flds datatype=[any]>.. <#LF><#flds/>
Merk at <#LF> produserer en line feed..
*/

const template = `
using system;

namespace <#Namespace>.<#Tablename>
{
    public class <#Tablename>Dto
    {
<#flds datatype=[any]>        public <#datatype> <#fieldname> {get; set;}<#LF><#flds/>
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'Customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true },
        { 'fieldname': 'Firstname', 'datatype': 'String' },
        { 'fieldname': 'LastName', 'datatype': 'String' }
    ]
}

test('Csharp add properties', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})