using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace mka.com.employee
{
    public class Employee
    {
        public enum EmployeeFields
        {
            Id
            , Name
            , Age
            , Income
            , Born

        }       

		
        private Integer _id;
        public Integer Id { get => _id; set { _id = value; IsDirty = true; } }
		
        private String _name;
        public String Name { get => _name; set { _name = value; IsDirty = true; } }
		
        private Integer _age;
        public Integer Age { get => _age; set { _age = value; IsDirty = true; } }
		
        private Decimal _income;
        public Decimal Income { get => _income; set { _income = value; IsDirty = true; } }
		
        private DateTime _born;
        public DateTime Born { get => _born; set { _born = value; IsDirty = true; } }
		
        // Helper var to display if data has changed since retrieved from db 
        [NotMapped]
        public Boolean IsDirty { get; set; }
    
        // Helper var to use i selection from grid is needed
        [NotMapped]
        public Boolean IsSelected { get; set; }

        // Helper objects, can contain anything
        [NotMapped]
        public Object HelperObj1 { get; set; }
        
        [NotMapped]
        public Object HelperObj2 { get; set; }
                
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            if (Id != null)
                result.Append("Id:").AppendLine(Id.ToString());

            if (Name != null)
                result.Append("Name:").AppendLine(Name.ToString());

            if (Age != null)
                result.Append("Age:").AppendLine(Age.ToString());

            if (Income != null)
                result.Append("Income:").AppendLine(Income.ToString());

            if (Born != null)
                result.Append("Born:").AppendLine(Born.ToString());
          
            return result.ToString();
        }
    }
}