const fs = require('fs')
const templateGenerator = require('./src/templateGenerator.js')
//const templateGenerator = require('./templateGenerator.js')

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

/*

fs.mkdir('./tmp/a/apple', { recursive: true }, (err) => {
  if (err) throw err;
});*/

const CreateStructure = folderStructure => {
    console.log('creating folder: ' + folderStructure)
    fs.mkdir(folderStructure, { recursive: true }, (err) => { if (err) throw err; })
}

let tableDef = {
    'fileName': 'employee',
    'nameSpace': 'mka.com',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true, 'isNullable': false },
        { 'fieldname': 'Name', 'datatype': 'String', 'isId': false },
        { 'fieldname': 'Age', 'datatype': 'Integer', 'isId': false },
        { 'fieldname': 'Income', 'datatype': 'Decimal', 'isId': false },
        { 'fieldname': 'Born', 'datatype': 'DateTime', 'isId': false }
    ]
}

console.log('')
console.log('---------------------------------------------')
console.log('Creating output structure...')
console.log('---------------------------------------------')
CreateStructure('./output/')
CreateStructure('./output/net')
CreateStructure('./output/net/employee')


let files = [
    { template: './templates/dotnet/csDtoSS.txt', output: './output/net/_test_/_Test_Dto.cs' },    
]

console.log('')
console.log('---------------------------------------------')
console.log('Generation of code...')
console.log('---------------------------------------------')



files.map(f => {
    let outFile = f.output.replace(/_test_/g, tableDef.fileName).replace(/_Test_/g, capitalize(tableDef.fileName))
    let template = fs.readFileSync(f.template, 'utf8')
    fs.writeFileSync(outFile, templateGenerator.generate(tableDef, template))
    console.log(outFile)
})
