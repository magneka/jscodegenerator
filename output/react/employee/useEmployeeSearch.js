// Logic komponent for employeeContainer

import React, { useState } from 'react';
import { mapFromDatabase, mapToDatabase } from './DataMappers'
import { FormattedMessage } from 'react-intl';
import useRestApi from '../../common/utilities/axiosHook/useRestApiHook'
import { toast } from "react-toastify";

export const EMPLOYEE_API = 'http://localhost:5000/Employee'

const useEmployee = (() => {

  const [currentRecord, setCurrentRecord] = useState('')

  const mapFromDb = (employee) => {
    var res = mapFromDatabase(employee)
    if (res.length === 1)
      setCurrentRecord(res[0])
    return res;
  }

  // Maps JSON object from form model to database model
  const mapToDb = (employee) => {
    return mapToDatabase(employee)
  }

  // Returns empty form
  const mapEmptyForm = () => {
    return {}
  }

  // connects to general hook for rest api usage
  const [state, setOperation] = useRestApi(EMPLOYEE_API, mapToDb, mapFromDb, mapEmptyForm)

  const search = (id, name, messageOk, messageNotFound, restApiError) => {
    //console.log('search: ', id, name,  messageOk, messageNotFound, restApiError)
    setCurrentRecord('')
    if (name) {
      setOperation({
        'restMethod': 'GET',
        'extraUri': '/Search/' + name,
        'id': '',
        messageOk,
        messageNotFound,
        restApiError
      })
    } else {
      setOperation({
        'restMethod': 'GET',        
        'id': id,
        messageOk,
        messageNotFound,
        restApiError
      })
    }
  }

  const clearForm = () => {
    console.log ("Clearing state")
    setOperation({
      'restMethod': 'CLEARSTATE',      
      'id': '',      
    })
  }

  return [search, clearForm, state, currentRecord, setCurrentRecord]

})

export default useEmployee