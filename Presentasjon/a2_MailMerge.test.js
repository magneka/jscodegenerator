const templFuncs = require('../src/templateFunctions.js')

/* 
Et av problemene med kode, er at noen steder skal ting skrives med spesifikk casing.
Vi kan suffixe tags med:
  UpperCase first  .._UF>
  LowerCase first  .._LF>
  Uppercase All    .._UA>
  LowerCase All    .._LA> 
*/

const template = `
TIl:         <#fornavn_UF> <#etternavn_UF>
Vedrørende:  <#emne_UA>

Du er hjertelig velkommen til møte om "<#emne_LA>" som du utfordret meg på.

hilsen
<#avsender_UF>`

let metaData = {
    'fornavn': 'torodd',
    'etternavn': 'Tomte Knudsen',
    'emne': 'Automatisk generering av KODE',
    'avsender': 'magne'
}

test('Mailmerge with casing', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})