const fs = require('fs')
const templateGenerator = require('./src/templateGenerator.js')

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const CreateStructure = folderStructure => {
  console.log('creating folder: ' + folderStructure)
  fs.mkdir(folderStructure, { recursive: true }, (err) => {if (err) throw err;})
}

let tableDef = {
  'fileName': 'employee',
  'nameSpace': 'mka.com',
  'fields': [
    { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true, 'isNullable': false },
    { 'fieldname': 'Name', 'datatype': 'String', 'isId': false },
    { 'fieldname': 'Age', 'datatype': 'Integer', 'isId': false },
    { 'fieldname': 'Income', 'datatype': 'Decimal', 'isId': false },
    { 'fieldname': 'Born', 'datatype': 'DateTime', 'isId': false }
  ]
}

const getTablesort = datatype => {
  switch (datatype) {
    case 'Integer': 
    case 'Decimal':
        return 'num'  
    case 'DateTime':
      return 'datetime' 
    default:
      return 'string';
  }
}

// Add tablesort prop
tableDef.fields.map(field => {
  field.TableSort = getTablesort(field.datatype)
})

console.log('')
console.log('---------------------------------------------')
console.log( 'Creating output structure...')
console.log('---------------------------------------------')
CreateStructure('./output/')
CreateStructure('./output/react')
CreateStructure('./output/react/employee')
CreateStructure('./output/react/employee/details')
CreateStructure('./output/react/employee/searchQuery')
CreateStructure('./output/react/employee/searchResults')
CreateStructure('./output/react/employee/edit')

let files = [
  {template: './templates/react/_Test_Container.txt', output: './output/react/_test_/_Test_Container.js'},
  {template: './templates/react/_test_Container.I18n.txt', output: './output/react/_test_/_test_Container.I18n.js'},
  
  {template: './templates/react/use_Test_Search.txt', output: './output/react/_test_/use_Test_Search.js'},

  {template: './templates/react/details/reactSrchDetComponentI18n.txt', output: './output/react/_test_/details/_test_DetailsComponent.I18n.js'},
  {template: './templates/react/details/reactSrchDetComponent.txt',     output: './output/react/_test_/details/_Test_DetailsComponent.js'},

  {template: './templates/react/SearchQuery/reactSrcQueryComponent.txt', output: './output/react/_test_/searchQuery/_Test_QueryComponent.js'},
  {template: './templates/react/SearchQuery/reactSrcQueryComponenti18n.txt', output: './output/react/_test_/searchQuery/_test_QueryComponentI18n.js'},

  {template: './templates/react/SearchResults/reactSearchResultsComponent.txt', output: './output/react/_test_/searchResults/_Test_SearchResultsComponent.js'},
  {template: './templates/react/SearchResults/reactSearchResultsComponent.I18n.txt', output: './output/react/_test_/searchResults/_test_SearchResultsComponent.I18n.js'},

  {template: './templates/react/Edit/reactEditTemplate.txt', output: './output/react/_test_/edit/_Test_EditComponent.js'},
  {template: './templates/react/Edit/reactEditTemplate.I18n.txt', output: './output/react/_test_/edit/_test_EditComponent.I18n.js'}

]

console.log('')
console.log('---------------------------------------------')
console.log( 'Generation of code...')
console.log('---------------------------------------------')



files.map(f => {  
  let outFile = f.output.replace(/_test_/g, tableDef.fileName).replace(/_Test_/g, capitalize(tableDef.fileName))
  let template = fs.readFileSync(f.template, 'utf8')
  fs.writeFileSync(outFile, templateGenerator.generate(tableDef, template))
  console.log( outFile)
})
