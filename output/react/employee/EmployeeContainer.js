import React, { Fragment, useState } from 'react';
import { useForm } from 'react-hook-form'
import { Alert } from 'react-bootstrap';

import WindowContainer from '../../common/layoutComponents/widgets/WindowContainer';
import { IntlProvider } from 'react-intl';

import util from 'util'

import LocaleContext from '../../common/contexts/LocaleContext';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { messages } from './employeeContainer.i18n'
import useEmployeeSearch from './useEmployeeSearch'
import EmployeeDetailsComponent from './details/EmployeeDetailsComponent'
import EmployeeSearchResultsComponent from './SearchResults/EmployeeSearchResultsComponent'
import EmployeeSearchQueryComponent from './SearchQuery/EmployeeSearchQueryComponent'

const EmployeeContainer = () => {

  const locale = React.useContext(LocaleContext);
  const [search, clearForm, state, 
    currentEmployee, setCurrentEmployee] = useEmployeeSearch()

  const showDetails = () => (currentEmployee)
  const showSearchResults = () => (Array.isArray(state.data)) && (state.data.length > 1)
  const showQuery = () => (state.data)

  return (

    <IntlProvider locale={locale} messages={messages[locale]}>

      <ToastContainer autoClose={4000} />     
             
      {(showQuery() && (!showDetails() && !showSearchResults())) && 
        <EmployeeSearchQueryComponent
        locale={locale}
        state={state}
        search={search}
        clearForm={clearForm}        
      />}         

      {showSearchResults() && !showDetails() &&
        <EmployeeSearchResultsComponent locale={locale} data={state.data} setCurrent={setCurrentEmployee} clearForm={clearForm}/>
      }
      {/*  */}
    
      {showDetails() &&
        <EmployeeDetailsComponent locale={locale} data={currentEmployee} setCurrent={setCurrentEmployee}/>
      }


    </IntlProvider >
  )

}

export default EmployeeContainer