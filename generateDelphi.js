const fs = require('fs')
const templateGenerator = require('./src/templateGenerator.js')

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

const CreateStructure = folderStructure => {
    console.log('creating folder: ' + folderStructure)
    fs.mkdir(folderStructure, { recursive: true }, (err) => { if (err) throw err; })
}

let tableDef = {
    'fileName': 'employee',
    'nameSpace': 'mka.com',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'SQLtype': 'INTEGER', 'isId': true, 'isNullable': false, 'NotNull': 'NOT NULL' },
        { 'fieldname': 'Name', 'datatype': 'String', 'SQLtype': 'VARCHAR(80)', 'isId': false, 'NotNull': 'NOT NULL'  },
        { 'fieldname': 'Age', 'datatype': 'Integer', 'SQLtype': 'SMALLINT', 'isId': false, 'NotNull': ''  },
        { 'fieldname': 'Income', 'datatype': 'Decimal', 'SQLtype': 'DECIMAL(10,2)', 'isId': false, 'NotNull': ''  },
        { 'fieldname': 'Born', 'datatype': 'DateTime', 'SQLtype': 'DATETIME', 'isId': false, 'NotNull': ''  }
    ]
}

console.log('')
console.log('---------------------------------------------')
console.log('Creating output structure...')
console.log('---------------------------------------------')
CreateStructure('./output/')
CreateStructure('./output/delphi')
CreateStructure('./output/delphi/employee')

let files = [
    { template: './templates/delphi/DAO_Base_Template.txt', output: './output/delphi/_test_/_Test_DAO_Base.pas' },
]

console.log('')
console.log('---------------------------------------------')
console.log('Generation of code...')
console.log('---------------------------------------------')

files.map(f => {
    let outFile = f.output.replace(/_test_/g, tableDef.fileName).replace(/_Test_/g, capitalize(tableDef.fileName))
    let template = fs.readFileSync(f.template, 'utf8')
    fs.writeFileSync(outFile, templateGenerator.generate(tableDef, template))
    console.log(outFile)
})
