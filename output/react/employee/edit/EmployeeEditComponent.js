import React, { Fragment } from 'react';
import { useForm } from 'react-hook-form'
import {
    Alert, Form,
    Button,    
    Row, Col, Container
} from 'react-bootstrap';

import WindowContainer from '../../../common/layoutComponents/widgets/WindowContainer';
import {
    IntlProvider, FormattedMessage,
    //injectIntl
} from 'react-intl';

import { messages } from './employeeEditComponent.i18n'
import LocaleContext from '../../common/contexts/LocaleContext';
import styled from 'styled-components'
import useDemoFormOne from './DemoFormOne'

//import { toast } from "react-toastify";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const employeeEditComponent = () => {

    const locale = React.useContext(LocaleContext);

    const { register, getValues, setValue, errors, reset, watch, formState, handleSubmit } = useForm({ mode: "onChange" });

    const [saveForm, deleteForm, getById, clearForm, state, readonlyId] = useDemoFormOne (getValues, reset) 

    const onSubmit = (data, e) => { console.log(data, e.target.id) }
   
    return (
        <IntlProvider locale={locale} messages={messages[locale]}>
            <Fragment>                               
                <WindowContainer caption={<FormattedMessage id="caption" />}>

                    <ToastContainer autoClose={4000} />

                    {!(Object.keys(errors).length === 0 && errors.constructor === Object) && <UcFormError header={<FormattedMessage id="errorsCaption" />} message={<FormattedMessage id="errorsMessage" />} />}
                                        
                    <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">                        

                        {/* Primary key field */} 
                                                  
                        <UcContainer>
                            <UcRow>
                                <UcCol className='col-md-6'>

                                  <UcFormGroup
                                    name='id'                            
                                    label={<FormattedMessage id="id" />}
                                    errors={errors}
                                    information={<FormattedMessage id="idInfo" />}
                                    errorMessage={<FormattedMessage id="idError" />}
                                >

                                    <Input
                                        className="form-control col-md-2"
                                        id='id'
                                        name="id"
                                        readOnly={readonlyid}
                                        ref={register()}
                                        type="number"
                                        min="0"
                                        step="1"
                                        style={errors['id'] && { border: '1px solid #f00' }}
                                        onKeyPress={e => {
                                            if (e.key === 'Enter') {
                                                getById()
                                            }                                                                        
                                        }}
                                    /> 
                                  </UcFormGroup>
                                    
                                </UcCol>                                
                            </UcRow>
                        </UcContainer>
                        

                        {/* All the non Primary key fields */}    
                          
                        <UcContainer>
                            <UcRow>
                                <UcCol className='col-md-6'>

                                    <UcFormGroup
                                        name='name'
                                        label={<FormattedMessage id="name" />}
                                        errors={errors}
                                        information={<FormattedMessage id="nameInfo" />}
                                        errorMessage={<FormattedMessage id="nameError" />}
                                    >

                                        <input
                                            className="form-control"
                                            id='name'
                                            name="name"
                                            data-testid="name"
                                            ref={register({ required: true, minLength: 2, maxLength: 20 })}
                                            style={errors['name'] && { border: '1px solid #f00' }}
                                        />
                                    </UcFormGroup>

                                </UcCol>                                
                            </UcRow>
                        </UcContainer>
                           
                        <UcContainer>
                            <UcRow>
                                <UcCol className='col-md-6'>

                                    <UcFormGroup
                                        name='age'
                                        label={<FormattedMessage id="age" />}
                                        errors={errors}
                                        information={<FormattedMessage id="ageInfo" />}
                                        errorMessage={<FormattedMessage id="ageError" />}
                                    >

                                        <input
                                            className="form-control"
                                            id='age'
                                            name="age"
                                            data-testid="age"
                                            ref={register({ required: true, minLength: 2, maxLength: 20 })}
                                            style={errors['age'] && { border: '1px solid #f00' }}
                                        />
                                    </UcFormGroup>

                                </UcCol>                                
                            </UcRow>
                        </UcContainer>
                           
                        <UcContainer>
                            <UcRow>
                                <UcCol className='col-md-6'>

                                    <UcFormGroup
                                        name='income'
                                        label={<FormattedMessage id="income" />}
                                        errors={errors}
                                        information={<FormattedMessage id="incomeInfo" />}
                                        errorMessage={<FormattedMessage id="incomeError" />}
                                    >

                                        <input
                                            className="form-control"
                                            id='income'
                                            name="income"
                                            data-testid="income"
                                            ref={register({ required: true, minLength: 2, maxLength: 20 })}
                                            style={errors['income'] && { border: '1px solid #f00' }}
                                        />
                                    </UcFormGroup>

                                </UcCol>                                
                            </UcRow>
                        </UcContainer>
                           
                        <UcContainer>
                            <UcRow>
                                <UcCol className='col-md-6'>

                                    <UcFormGroup
                                        name='born'
                                        label={<FormattedMessage id="born" />}
                                        errors={errors}
                                        information={<FormattedMessage id="bornInfo" />}
                                        errorMessage={<FormattedMessage id="bornError" />}
                                    >

                                        <input
                                            className="form-control"
                                            id='born'
                                            name="born"
                                            data-testid="born"
                                            ref={register({ required: true, minLength: 2, maxLength: 20 })}
                                            style={errors['born'] && { border: '1px solid #f00' }}
                                        />
                                    </UcFormGroup>

                                </UcCol>                                
                            </UcRow>
                        </UcContainer>
                         

                        {/* Skal være gyldig ved tom form, ikke readonlyId */}
                        <Button onClick={() => getById()} variant="primary" disabled={state.isLoading || readonlyId !== false}>{<FormattedMessage id="getButton" />}</Button>&nbsp;

                        {/* Skal være gyldig ved valid form og ikke loading - OK*/}
                        <Button onClick={() => saveForm()} variant="primary" disabled={state.isLoading || !formState.dirty || (formState.dirty && !formState.isValid)}>{<FormattedMessage id="saveButton" />}</Button>&nbsp;
                        
                        {/* Skal være gyldig readonlyId og ikke loading */}
                        <Button onClick={() => deleteForm()} variant="primary" disabled={state.isLoading || readonlyId === false}>{<FormattedMessage id="deleteButton" />}</Button>&nbsp;
                        
                        {/* Skal ikke være gyldig ved loading */}
                        <Button onClick={() => clearForm()} variant="primary" disabled={state.isLoading}>{<FormattedMessage id="clearButton" />}</Button>&nbsp;                       

                    </form>

                </WindowContainer>
                
                {/* 
                <div>Reducer State:{util.inspect(state)}</div>
                <div>Form errors:{util.inspect(errors)}</div>
                <div>Formstate:{util.inspect(formState)}</div>
                */}


            </Fragment>
        </IntlProvider >
    )
}

export default employeeEditComponent

const UcFormError = (props) => {
    return (
        <Alert variant='danger'><Alert.Heading>{props.header}</Alert.Heading>{props.message}</Alert>
    )
}

const UcFormGroup = (props) => {
    return (
        <Form.Group controlId={props.name}>
            <Form.Label>{props.label}</Form.Label>
            {props.children}
            {props.information && <Form.Text className="text-muted">{props.information}</Form.Text>}
            {props.errors[props.name] && <span data-testid={props.name + '_err'} style={{ color: 'red' }}>{props.errorMessage}</span>}
        </Form.Group>
    )
}

const UcContainer = styled(Container)`
    margin-left: 0px;
    padding-left: 15px;
`

const UcRow = styled(Row)`    
    padding-left: 0px;
`

const UcCol = styled(Col)`    
    padding-left: 0px;
`

const Input = styled.input`
    ::-webkit-inner-spin-button{
        -webkit-appearance: none; 
        margin: 0; 
    }
    ::-webkit-outer-spin-button{
        -webkit-appearance: none; 
        margin: 0; 
    }    
`;

