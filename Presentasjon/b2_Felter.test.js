const templFuncs = require('../src/templateFunctions.js')

/*
I dette eksemplet skal Id feltet annoteres, dvs at id vs non id felter 
må behandles forskjellig.  Til dette bruker vi Constraints 
for å inkludere/ekskludere: 
  isId/isPk, isNotPk, isNullable
*/

const template = `
using system;
using System.ComponentModel.DataAnnotations;

namespace <#Namespace>.<#Tablename>
{
    public class <#Tablename>Dto
    {
<#flds datatype=[any] isPk>
        [Key]
        public <#datatype> <#fieldname> {get; set;}<#LF>
<#flds/>
<#flds datatype=[any] isNotPk>        public <#datatype> <#fieldname> {get; set;}<#LF></#flds>
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'Customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true },
        { 'fieldname': 'Firstname', 'datatype': 'String' },
        { 'fieldname': 'LastName', 'datatype': 'String' }
    ]
}

test('CSharp annotate ID field', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})