const templFuncs = require('../src/templateFunctions.js')

// Her er de tidligere nevnte brukt i et kode eksempel (C#)
// Her ser vi skjelettet av en datatransfer klasse

const template = `
using system;

namespace <#Namespace>.<#Tablename>
{
    public class <#Tablename>Dto
    {    
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'Customer'
}

test('CSharp dte skeleton', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})