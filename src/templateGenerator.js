const  templFuncs = require('./templateFunctions.js')

/* eslint-disable no-useless-escape */
// eslint-disable-next-line no-extend-native
String.prototype.replaceAll = function (strReplace, strWith) {
  // See http://stackoverflow.com/a/3561711/556609
  var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
  var reg = new RegExp(esc, 'ig')
  return this.replace(reg, strWith)
}

exports.generate = (tableInfo, sourceCodeTemplate) => {
    
  /*
  const resolveAll = (table, sourceCode) => {
    let result = fixCompabilityIssues(sourceCode)

    while ((result.includes(startTag(FLDS))) && (result.includes(endTag(FLDS)))) {
      let a, b, template, parms
      [a, parms, template, b] = getSplitInnerSection(result, FLDS)

      let datatypes = getDataTypes(parms, 'datatype')
      let constraints = getConstraints(parms)

      let resolvedTemplate = resolveOneTemplate(table, template, datatypes, constraints)

      result = a + resolvedTemplate + b
    }

    result = replaceVariants(result, 'FileName', table.fileName)
    result = replaceVariants(result, 'Namespace', table.nameSpace)

    return result
  }*/

  return templFuncs.resolveAll(tableInfo, sourceCodeTemplate)
}
