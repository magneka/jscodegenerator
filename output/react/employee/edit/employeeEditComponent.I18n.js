export const messages = {
  en: {
        caption: `Employee Form`,
      
        id: `id`,
        idInfo: `Enter id`,
        idError: `Unable to reatrieve Employee. Please enter a valid id, only numbers`, 
  
   
        name: `name`,          
        nameInfo: `Enter name`,  
        nameError: `This is a required field, it must contain between 2 and 20 characters`,  
  
        age: `age`,          
        ageInfo: `Enter age`,  
        ageError: `This is a required field, it must contain between 2 and 20 characters`,  
  
        income: `income`,          
        incomeInfo: `Enter income`,  
        incomeError: `This is a required field, it must contain between 2 and 20 characters`,  
  
        born: `born`,          
        bornInfo: `Enter born`,  
        bornError: `This is a required field, it must contain between 2 and 20 characters`,  
                
        errorsCaption: `Unable to save data`,  
        errorsMessage: `There are ivalid data in fields, or required fields not filled in.`, 
        getButton: `Retrieve`, 
        saveButton: `Save`, 
        deleteButton: `Delete`, 
        clearButton: `Clear form`, 
        getOkMessage: `Employee retrieved OK`,
        getNotFoundMessage: `Employee not found in database`,
        insertedMessage: `New Employee created in database`,
        updatedMessage: `Employee is updated in database`,
        deletedMessage: `Employee is permanently deleeted from the database`,
        restApiError: `Error: server did not respond as expected`, 
  },
  nb: {
      caption: `Registrering og endring av Employee eksempel`,
      
      id: `id`,
      idInfo: `Oppgi id`,
      idError: `Kan ikke laste Employee> fra databasen. Vennligst oppgi kun tall i id feltet.`, 
      

   
      name: `name`,  
      nameInfo: `Oppgi name`,
      nameError: `Dette feltet er påkrevd, og må må inneholde mellom 2 og 20 tegn`,  
   
      age: `age`,  
      ageInfo: `Oppgi age`,
      ageError: `Dette feltet er påkrevd, og må må inneholde mellom 2 og 20 tegn`,  
   
      income: `income`,  
      incomeInfo: `Oppgi income`,
      incomeError: `Dette feltet er påkrevd, og må må inneholde mellom 2 og 20 tegn`,  
   
      born: `born`,  
      bornInfo: `Oppgi born`,
      bornError: `Dette feltet er påkrevd, og må må inneholde mellom 2 og 20 tegn`,  
          
      errorsCaption: `Data kan ikke lagres`,
      errorsMessage: `Det er felter med ugyldige data eller påkrevde felter som ikke er utfylt.`, 
      getButton: `Hent data`,
      saveButton: `Lagre`,
      deleteButton: `Slett`,
      clearButton: `Tøm felter`, 
      getOkMessage: `Employee hentet fra databasen`,
      getNotFoundMessage: `Employee ikke funnet i databasen`,
      insertedMessage: `Ny Employee er opprettet i databasen`, 
      updatedMessage: `Employee er lagret i databasen`, 
      deletedMessage: `Employee er slettet fra databasen`, 
      restApiError: `FEIL: noe gikk galt på serveren...`, 

  }
}

    