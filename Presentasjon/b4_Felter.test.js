const templFuncs = require('../src/templateFunctions.js')

// I dette eksemplet skal vi lage en constructor, dat trenger vi 
// en metodedeklarering der alle felter skal med, samt metode body med tildelinger

const template = `
using system;
using System.ComponentModel.DataAnnotations;

namespace <#Namespace>.<#Tablename>
{
    public class <#Tablename_UF>Dto
    {
<#flds datatype=[any] isPk>
        [Key]
        public <#datatype> <#fieldname> {get; set;}<#LF>
<#flds/>
<#flds datatype=[any] isNotPk>        public <#datatype> <#fieldname> {get; set;}<#LF></#flds>

        // Constructor
        public <#Tablename_UF>Dto(<#flds datatype=[any]><#KOMMA> <#datatype> <#fieldname_LF></#flds>)
        {
<#flds datatype=[any]>            <#fieldname_UF> = <#fieldname_LF>;<#LF></#flds>
        }
    }
}
`

let metaData = {
    'Namespace': 'Source.Datalayer.Repositories',
    'Tablename': 'customer',
    'fields': [
        { 'fieldname': 'Id', 'datatype': 'Integer', 'isId': true },
        { 'fieldname': 'Firstname', 'datatype': 'String' },
        { 'fieldname': 'LastName', 'datatype': 'String' }
    ]
}

test('CSharp constructor', () => {
    let res = templFuncs.resolveAll(metaData, template)
    console.log(res)
})