export const messages = {
  en: {
    caption: `Employee`,
    okmessage: `Data retrieved OK`,
    notfoundmessage: `Data not found in database`,
    restapierror: `Error: server did not respond as expected`,
  },
  nb: {
    caption: `Employee`,
    okmessage: `Data hentet fra databasen`,
    notfoundmessage: `Data ikke funnet i databasen`,
    restapierror: `FEIL: noe gikk galt på serveren...`,
  }
}
